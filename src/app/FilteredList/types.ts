export type CREATE_ITEM = 'FilteredList/CREATE_ITEM'
export const CREATE_ITEM: CREATE_ITEM = 'FilteredList/CREATE_ITEM'

export type DELETE_ITEM = 'FilteredList/DELETE_ITEM'
export const DELETE_ITEM: DELETE_ITEM = 'FilteredList/DELETE_ITEM'

interface CreateItemAction {
    type: CREATE_ITEM
    payload: string
}

interface DeleteItemAction {
    type: DELETE_ITEM
}

export type FilteredListAction = CreateItemAction | DeleteItemAction

export interface FilteredListState {
    items: Array<string>
}
