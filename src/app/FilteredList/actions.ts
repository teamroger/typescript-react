import { CREATE_ITEM, DELETE_ITEM, FilteredListAction } from './types';

export function createItem(item: string): FilteredListAction {
    return {
        type: CREATE_ITEM,
        payload: item
    };
}

export function deleteItem(): FilteredListAction {
	return {
        type: DELETE_ITEM,
    }
}
