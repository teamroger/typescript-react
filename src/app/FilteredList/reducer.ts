import {
    CREATE_ITEM,
    DELETE_ITEM,
    FilteredListAction,
    FilteredListState
} from './types'

const initialState: FilteredListState = {
    items: []
}

export default function filteredListReducer(
    state: FilteredListState = initialState,
    action: FilteredListAction
): FilteredListState {
	switch(action.type) {
		case CREATE_ITEM:
			return {
				...state,
				items: [
                    ...state.items,
                    action.payload
                ]
			};
		
        case DELETE_ITEM:
            return {
                items: state.items.slice(0, -1)
            }
		
		default:
			return state;
	}
}
