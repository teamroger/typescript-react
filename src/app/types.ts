import { Action } from 'redux';

export interface IState {
    coolList: {
        text: string
    };

    searchBar: {
        text: string,
        matches: Array<string>
    };

    items: Array<string>;
}

export interface IAction<P> extends Action {
    payload: P;
}
