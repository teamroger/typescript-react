import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CoolListContainer from './components/CoolList/CoolList.container';
import SearchBarContainer from './components/SearchBar/SearchBar.container';

class App extends Component {
  render() {
    return (
      <div className="App">
        <CoolListContainer />

		<SearchBarContainer />
      </div>
    );
  }
}

export default App;
