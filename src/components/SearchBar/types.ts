export interface SearchBarState {
    items: Array<string>
}

export interface DataProps {
	items: Array<string>
}

export interface FunctionProps {
    
}

export interface ComponentProps extends DataProps, FunctionProps {
	
}
