import * as React from 'react'
import {connect} from 'react-redux'
import SearchBar from './SearchBar'
import { FunctionComponent } from 'react'
import { SearchBarState, DataProps, FunctionProps, ComponentProps } from './types'

function mapState(state: SearchBarState): DataProps {
    return { items: state.items }
}

function mapDispatch(dispatch: Function): FunctionProps {
    return {}
}

const SearchBarContainer: FunctionComponent<ComponentProps> = (props: ComponentProps) => (
	<SearchBar items={ props.items } />
)

export default connect(mapState, mapDispatch)(SearchBarContainer)
