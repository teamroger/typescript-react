import './style.css'
import * as React from 'react'
import { Component, ChangeEvent } from 'react'

const filterMatches = (list: Array<string>, text: string): Array<string> => {
    return list.filter((item) => {
        return item.slice(0, text.length) === text
    })
}

interface IProps {
    items: Array<string>
}

interface IState {
    text: string
}

class SearchBar extends Component<IProps, IState> {
    public constructor(props: IProps) {
        super(props)

        this.state = {
            text: ''
        }

        this.searchBarChange = this.searchBarChange.bind(this)
    }

    protected searchBarChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState({
            text: e.target.value
        })
    }

    public render() {
        return (<div className="searchBar">
            <h2>Search Bar</h2>
            
            <p>
                <label htmlFor="searchBar">Search: </label>
                <input type="text" id="searchBar" value={ this.state.text } onChange={ this.searchBarChange } />
            </p>
            
            <ul>
                { filterMatches(this.props.items, this.state.text).map((item, key) => (
                    <li key={key}>{item}</li>
                )) }
            </ul>
        </div>)
    }
}

export default SearchBar
