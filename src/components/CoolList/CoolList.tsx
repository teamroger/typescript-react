import './style.css'
import * as React from 'react'
import { Component, ChangeEvent } from 'react'

interface IProps {
    items: Array<string>
    addItem: any
    removeItem: any
}

interface IState {
    text: string
}

class CoolList extends Component<IProps, IState> {
    public constructor(props: IProps) {
        super(props)

        this.state = { text: '' }

        this.textChange = this.textChange.bind(this)
    }

    protected textChange(e: ChangeEvent<HTMLInputElement>) {
        this.setState({ text: e.target.value })
    }

    public render() {
        return (
            <div className="coolList">
                <h2>Cool List</h2>
                
                <p>
                    <label htmlFor="itemToAdd">Item to add: </label>
                    <input type="text" id="itemToAdd" value={ this.state.text } onChange={ this.textChange } />
                </p>
                
                <button onClick={ this.props.addItem(this.state.text) }>New item</button>
                <button onClick={ this.props.removeItem }>Remove item</button>
                
                <ul>
                    { this.props.items.map((item: string, key: number) => (
                        <li key={ key }>{ item }</li>
                    )) }
                </ul>
            </div>
        )
    }
}

export default CoolList
