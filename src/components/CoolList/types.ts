import { ChangeEvent, MouseEvent } from 'react'

export interface CoolListState {
    items: Array<string>
}

export interface DataProps {
	items: Array<string>
}

export interface FunctionProps {
	addItem:    (text: string) => void
	removeItem: (event: MouseEvent<HTMLButtonElement>) => void
}

export interface ComponentProps extends DataProps, FunctionProps {
	
}
