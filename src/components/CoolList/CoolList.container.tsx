import * as React from 'react';
import CoolList from './CoolList'
import { connect } from 'react-redux'
import { FunctionComponent, MouseEvent } from 'react'
import { createItem, deleteItem } from '../../app/FilteredList/actions'
import { CoolListState, DataProps, FunctionProps, ComponentProps } from './types'

function mapState(state: CoolListState): DataProps {
    return { items: state.items }
}

function mapDispatch(dispatch: Function): FunctionProps {
    return {
        addItem: function (text: string): Function {
            return function(e: MouseEvent<HTMLButtonElement>): void {
                dispatch(createItem(text))
            }
        },
        removeItem: function (e: MouseEvent<HTMLButtonElement>): void {
            dispatch(deleteItem())
        }
    }
}

const CoolListContainer: FunctionComponent<ComponentProps> = (props: ComponentProps) => (
	<CoolList
		items={props.items}
		addItem={props.addItem}
		removeItem={props.removeItem} />
)

export default connect(mapState, mapDispatch)(CoolListContainer)
